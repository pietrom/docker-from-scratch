`gcc -o hello-from-scratch-with-envvar/hello/hello --static hello-from-scratch-with-envvar/hello/hello.c`

`docker build -t hello-from-scratch-with-envvar ./hello-from-scratch-with-envvar`

`docker run --rm --env TARGET=Pietro hello-from-scratch-with-envvar`

`docker run --rm -e TARGET=Pietro hello-from-scratch-with-envvar --env TARGET=Pietro`